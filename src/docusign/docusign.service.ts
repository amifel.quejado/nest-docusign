import { Injectable } from '@nestjs/common';
import { EnvelopesApi, TemplatesApi, FoldersApi } from 'docusign-esign';
import { readFileSync } from 'fs';
import { resolve } from 'path';
import { DocusignApiService } from './ds-api.service';
import { TemplateService } from './template.service';
import { EnvelopeService } from './envelope.service';

/**
 * TODO: Better error handling
 * Base here: https://developers.docusign.com/docs/esign-rest-api/esign101/status-and-error-codes/
 */
@Injectable()
export class DocusignService {
  constructor(
    private dsApiService: DocusignApiService,
    private templateService: TemplateService,
    private envelopeService: EnvelopeService,
  ) {}

  async login() {
    try {
      const absPath = resolve('src/docusign/private.key');
      const key = readFileSync(absPath, 'utf-8');

      // TODO: Create JWT auth module, strategy, service and guard
      const response = await this.dsApiService.dsApiClient.requestJWTUserToken(
        process.env.DS_CLIENT_ID,
        process.env.DS_IMPERSONATED_USER_GUID,
        'signature impersonation',
        key,
        parseInt(process.env.TOKEN_EXP),
      );

      // set token
      this.dsApiService.setBearerToken(response.body.access_token);

      return {
        access_token: response.body.access_token,
        expires: response.body.expires_in,
      };
    } catch (error) {
      console.log(error);

      const res = error.response && JSON.parse(error.response?.text);

      if (res && res.message === 'consent_required') {
        return {
          error: 'Consent needed',
          url: `${process.env.DS_AUTH_SERVER}/oauth/auth?response_type=code&scope=signature%20impersonation&client_id=${process.env.DS_CLIENT_ID}&redirect_uri=${process.env.DS_APP_URL}/ds/callback`,
        };
      }
      return { error: error.message };
    }
  }

  async getUserLoggedIn() {
    try {
      const response = await this.dsApiService.dsApiClient.getUserInfo(
        this.dsApiService.getBearerToken(),
      );

      return response;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text || error.response.error.text);
      return { error: res.error || res.message, status: error.status };
    }
  }

  /**
   * TODO: Generate separate module for templates
   * TEMPLATES
   */
  async getTemplate(templateId) {
    try {
      const templatesApi = new TemplatesApi(this.dsApiService.dsApiClient);
      const response = await templatesApi.get(
        process.env.DS_ACCOUNT_ID,
        templateId,
        null,
      );

      return response;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text || error.response.error.text);
      return { error: res.message };
    }
  }

  async downloadTemplateDocs(templateId, documentId) {
    try {
      const templatesApi = new TemplatesApi(this.dsApiService.dsApiClient);
      const response = await templatesApi.getDocument(
        process.env.DS_ACCOUNT_ID,
        templateId,
        documentId,
        {
          encrypt: 'false',
        },
      );

      return response;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text || error.response.error.text);
      return { error: res.message };
    }
  }

  async getAllTemplates() {
    try {
      const templatesApi = new TemplatesApi(this.dsApiService.dsApiClient);
      const response = await templatesApi.listTemplates(
        process.env.DS_ACCOUNT_ID,
        null,
      );

      return response;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text || error.response.error.text);
      return { error: res.message };
    }
  }

  async createTemplate(file) {
    try {
      const templatesApi = new TemplatesApi(this.dsApiService.dsApiClient);

      // create template
      const template = this.templateService.createTemplate(file);
      const results = await templatesApi.createTemplate(
        process.env.DS_ACCOUNT_ID,
        {
          envelopeTemplate: template,
        },
      );

      if (results.error) {
        console.log(results);
        return { error: 'Template not created' };
      }

      const response = await templatesApi.get(
        process.env.DS_ACCOUNT_ID,
        results.templateId,
        null,
      );

      return response;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text || error.response.error.text);
      return { error: res.message };
    }
  }

  async createTemplateWithTags(options) {
    try {
      const templatesApi = new TemplatesApi(this.dsApiService.dsApiClient);

      // create template
      const template = this.templateService.createTemplateTags(options);
      const results = await templatesApi.createTemplate(
        process.env.DS_ACCOUNT_ID,
        {
          envelopeTemplate: template,
        },
      );

      if (results.error) {
        console.log(results);
        return { error: 'Template not created' };
      }

      const response = await templatesApi.get(
        process.env.DS_ACCOUNT_ID,
        results.templateId,
        null,
      );

      return response;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text || error.response.error.text);
      return { error: res.message };
    }
  }

  async updateTemplateRecipients(templateId, options) {
    try {
      const templatesApi = new TemplatesApi(this.dsApiService.dsApiClient);

      const newRecipients = {
        signers: options.signers,
        ccs: options.ccs,
      };

      // overwrites recipients
      // option 1: use deleteRecipient from templates api
      const templateRecipients =
        this.templateService.updateTemplateRecipients(newRecipients);

      const results = await templatesApi.updateRecipients(
        process.env.DS_ACCOUNT_ID,
        templateId,
        {
          templateRecipients,
        },
      );

      if (results.error) {
        console.log(results);
        return { error: 'Template recipients not updated' };
      }

      return results;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text);
      return { error: res.message };
    }
  }

  async updateTemplate(templateId, options) {
    try {
      const templatesApi = new TemplatesApi(this.dsApiService.dsApiClient);

      const template = this.templateService.updateTemplate(options);

      const results = await templatesApi.update(
        process.env.DS_ACCOUNT_ID,
        templateId,
        {
          envelopeTemplate: template,
        },
      );

      if (results.error) {
        console.log(results);
        return { error: 'Template not updated' };
      }

      return results;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text);
      return { error: res.message };
    }
  }
  /**
   * 
   * @param id templateId or envelopeId - can expand to multiple Ids
   */
  async delete(id) {
    try {
      const foldersApi = new FoldersApi(this.dsApiService.dsApiClient);
      const results = await foldersApi.moveEnvelopes(
        process.env.DS_ACCOUNT_ID,
        'recyclebin',
        {
          foldersRequest: {
            envelopeIds: [id],
          },
        },
      );

      return results;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text);
      return { error: res.message };
    }
  }

  /**
   * TODO: Generate separate module for envelopes
   * ENVELOPES
   */

  async sendEnvelope(options) {
    const envelopesApi = new EnvelopesApi(this.dsApiService.dsApiClient);

    const envelope = this.envelopeService.createEnvelope(options);

    const results = await envelopesApi.createEnvelope(
      process.env.DS_ACCOUNT_ID,
      {
        envelopeDefinition: envelope,
      },
    );

    return results;
  }

  async getEnvelope(envelopeId) {
    try {
      const envelopesApi = new EnvelopesApi(this.dsApiService.dsApiClient);

      const results = await envelopesApi.getEnvelope(
        process.env.DS_ACCOUNT_ID,
        envelopeId,
      );

      return results;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text);
      return { error: res.message };
    }
  }

  // expand for options - sent / completed / delivered status
  async getAllEnvelopes(): Promise<any> {
    try {
      // list all folders
      const foldersApi = new FoldersApi(this.dsApiService.dsApiClient);
      const results = await foldersApi.list(process.env.DS_ACCOUNT_ID, {
        userFilter: 'all',
      });

      // map folders, get envelopes for each
      let envelopes = [];
      for (let index = 0; index < results.folders.length; index++) {
        const folder = results.folders[index];
        if (folder.type === 'draft' || folder.type === 'sentitems') {
          const foldersApi2 = new FoldersApi(this.dsApiService.dsApiClient);
          const items = await foldersApi2.listItems(
            process.env.DS_ACCOUNT_ID,
            folder.folderId,
            {
              includeItems: 'true',
            },
          );
          if (items.folders && items.folders[0].folderItems)
            envelopes = envelopes.concat(items.folders[0].folderItems);
        }
      }

      // FOR TESTING
      // const envelopes = await foldersApi.listItems(
      //   process.env.DS_ACCOUNT_ID,
      //   '9580df53-4c1b-4f88-b470-8d29eec379fe',
      //   {
      //     includeItems: 'true',
      //   }
      // );

      return envelopes;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text);
      return { error: res.message };
    }
  }

  async updateEnvelope(options) {
    try {
      const envelope = this.envelopeService.updateEnvelope(options);
      const envelopesApi = new EnvelopesApi(this.dsApiService.dsApiClient);
      const results = await envelopesApi.update(
        process.env.DS_ACCOUNT_ID,
        options.envelopeId,
        {
          envelope,
          resendEnvelope: true,
        },
      );

      return results;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text || error.response.error.text);
      return { error: res.message };
    }
  }

  async deleteEnvelope(envelopeId) {
    try {
      const envelope = this.envelopeService.purgeEnvelope();
      const envelopesApi = new EnvelopesApi(this.dsApiService.dsApiClient);
      const results = await envelopesApi.update(
        process.env.DS_ACCOUNT_ID,
        envelopeId,
        {
          envelope,
        },
      );

      return results;
    } catch (error) {
      console.log(error);

      const res = JSON.parse(error.response.text || error.response.error.text);
      return { error: res.error || res.message, status: error.status };
    }
  }
}
