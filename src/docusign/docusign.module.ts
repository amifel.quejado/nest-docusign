import { Module } from '@nestjs/common';
import { DocusignService } from './docusign.service';
import { DocusignController } from './docusign.controller';
import { DocusignApiService } from './ds-api.service';
import { TemplateService } from './template.service';
import { EnvelopeService } from './envelope.service';

@Module({
  controllers: [DocusignController],
  providers: [
    DocusignService,
    DocusignApiService,
    TemplateService,
    EnvelopeService,
  ],
  exports: [DocusignService],
})
export class DocusignModule {}
