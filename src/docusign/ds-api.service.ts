import { Injectable } from '@nestjs/common';
import { ApiClient } from 'docusign-esign';

@Injectable()
export class DocusignApiService {
  public dsApiClient;

  constructor() {
    this.dsApiClient = new ApiClient({
      basePath: process.env.BASE_PATH_DEV,
    });
    this.dsApiClient.setBasePath(process.env.BASE_PATH_DEV);
  }

  setBearerToken(token) {
    this.dsApiClient.addDefaultHeader('Authorization', `Bearer ${token}`);
  }

  getBearerToken() {
    return this.dsApiClient.defaultHeaders['Authorization'].split(' ')[1];
  }
}
