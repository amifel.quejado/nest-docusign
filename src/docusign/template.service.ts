import { Injectable } from '@nestjs/common';
import {
  Document,
  Signer,
  CarbonCopy,
  SignHere,
  Tabs,
  Recipients,
  EnvelopeTemplate,
  TemplateRecipients,
  Text,
  Checkbox,
  Radio,
  RadioGroup,
} from 'docusign-esign';

@Injectable()
export class TemplateService {
  createTemplate(file) {
    // create doc, can be multiple docs
    const doc = new Document();
    doc.documentBase64 = file;
    doc.name = 'Demo file';
    doc.fileExtension = 'pdf';
    doc.documentId = '1';

    // create signer
    const signer1 = new Signer();
    signer1.roleName = 'signer';
    signer1.recipientId = '1';
    signer1.routingOrder = '1';
    // signer1.email = 'devaqaq@gmail.com';

    // via setters - lib/esignature/examples/createTemplate
    // create cc - will receive email when signer has completed the document
    const cc1 = new CarbonCopy();
    cc1.roleName = 'cc';
    cc1.routingOrder = '2';
    cc1.recipientId = '2';
    // cc1.email = 'amifelq.zeniark@gmail.com';

    // via constructFromObject method
    // create signature object for signer1
    const signHere = SignHere.constructFromObject({
      documentId: '1',
      pageNumber: '1',
      xPosition: '59', // x position in 72 DPI
      yPosition: '180', // y position in 72 DPI
    });

    const field1 = Text.constructFromObject({
      documentId: '1',
      pageNumber: '1',
      xPosition: '50', // x position in 72 DPI
      yPosition: '15', // y position in 72 DPI
      fontSize: 'size11',
      width: '118',
      height: '13',
      required: 'true',
      tabLabel: 'field1_text',
    });

    const field2 = Text.constructFromObject({
      documentId: '1',
      pageNumber: '1',
      xPosition: '50', // x position in 72 DPI
      yPosition: '53', // y position in 72 DPI
      fontSize: 'size11',
      width: '118',
      height: '13',
      required: 'true',
      tabLabel: 'field2_text',
    });

    const field3 = Text.constructFromObject({
      documentId: '1',
      pageNumber: '1',
      xPosition: '50', // x position in 72 DPI
      yPosition: '91', // y position in 72 DPI
      fontSize: 'size11',
      width: '118',
      height: '13',
      locked: 'true',
      value: 'Sample prefilled text',
      tabLabel: 'field3_text',
    });

    const chkBox1 = Checkbox.constructFromObject({
        documentId: '1',
        pageNumber: '1',
        xPosition: '50', // x position in 72 DPI
        yPosition: '129', // y position in 72 DPI
        fontSize: 'size11',
        width: '8', // or 8
        height: '8',
        tabLabel: 'field4_checkbox1',
      }),
      chkBox2 = Checkbox.constructFromObject({
        documentId: '1',
        pageNumber: '1',
        xPosition: '64', // x position in 72 DPI
        yPosition: '129', // y position in 72 DPI
        fontSize: 'size11',
        width: '8', // or 8
        height: '8',
        tabLabel: 'field4_checkbox2',
      }),
      chkBox3 = Checkbox.constructFromObject({
        documentId: '1',
        pageNumber: '1',
        xPosition: '78', // x position in 72 DPI
        yPosition: '129', // y position in 72 DPI
        fontSize: 'size11',
        width: '8', // or 8
        height: '8',
        tabLabel: 'field4_checkbox3',
      }),
      chkBox4 = Checkbox.constructFromObject({
        documentId: '1',
        pageNumber: '1',
        xPosition: '92', // x position in 72 DPI
        yPosition: '129', // y position in 72 DPI
        fontSize: 'size11',
        width: '8', // or 8
        height: '8',
        tabLabel: 'field4_checkbox4',
      }),
      chkBox5 = Checkbox.constructFromObject({
        documentId: '1',
        pageNumber: '1',
        xPosition: '105', // x position in 72 DPI
        yPosition: '129', // y position in 72 DPI
        fontSize: 'size11',
        width: '8', // or 8
        height: '8',
        tabLabel: 'field4_checkbox5',
      }),
      chkBox6 = Checkbox.constructFromObject({
        documentId: '1',
        pageNumber: '1',
        xPosition: '118', // x position in 72 DPI
        yPosition: '129', // y position in 72 DPI
        fontSize: 'size11',
        width: '8', // or 8
        height: '8',
        tabLabel: 'field4_checkbox6',
        selected: 'true',
      });

    const radioGroup = RadioGroup.constructFromObject({
      documentId: '1',
      groupName: 'field5_radio1',
      radios: [
        Radio.constructFromObject({
          pageNumber: '1',
          value: 'white',
          xPosition: '50', // x position in 72 DPI
          yPosition: '161', // y position in 72 DPI
          fontSize: 'size11',
          width: '8', // or 8
          height: '8',
          required: 'false',
        }),
        Radio.constructFromObject({
          pageNumber: '1',
          value: 'red',
          xPosition: '64', // x position in 72 DPI
          yPosition: '161', // y position in 72 DPI
          fontSize: 'size11',
          width: '8', // or 8
          height: '8',
          required: 'false',
        }),
        Radio.constructFromObject({
          pageNumber: '1',
          value: 'blue',
          xPosition: '78', // x position in 72 DPI
          yPosition: '161', // y position in 72 DPI
          fontSize: 'size11',
          width: '8', // or 8
          height: '8',
          required: 'false',
        }),
      ],
    });
    // create tabs for signer1
    const signer1Tabs = Tabs.constructFromObject({
      signHereTabs: [signHere],
      textTabs: [field1, field2, field3],
      checkboxTabs: [chkBox1, chkBox2, chkBox3, chkBox4, chkBox5, chkBox6],
      radioGroupTabs: [radioGroup],
    });
    signer1.tabs = signer1Tabs;

    // add recipients
    const recipients = Recipients.constructFromObject({
      signers: [signer1],
      carbonCopies: [cc1],
    });

    // overall template definition
    const template = new EnvelopeTemplate.constructFromObject({
      // The order in the docs array determines the order in the env
      documents: [doc],
      emailSubject: 'Please sign sample document', // email subject when received
      description: 'Demo template created via the NestJS docusign API', // template description
      name: 'Demo template', // template name
      shared: 'false',
      recipients: recipients,
      status: 'created', // * sent - The envelope is sent to the recipients.  * created - The envelope is saved as a draft and can be modified and sent later.
    });

    return template;
  }

  createTemplateTags(options) {
    // create doc, can be multiple docs
    const doc = new Document();
    doc.documentBase64 = options.file;
    doc.name = options.docName;
    doc.fileExtension = 'pdf';
    doc.documentId = '1';

    const parsedSigners = JSON.parse(options.signers);
    const signers = [];
    for (let index = 0; index < parsedSigners.length; index++) {
      const signer = parsedSigners[index];
      const signer1 = new Signer();
      signer1.roleName = 'signer' + index;
      signer1.name = signer.name;
      signer1.email = signer.email;
      signer1.recipientId = `${index + 1}`;
      signer1.routingOrder = `${index + 1}`;

      if (signer1.roleName === 'signer0') {
        // via constructFromObject method
        // create signature object for signer1
        const signHere = SignHere.constructFromObject({
          documentId: '1',
          pageNumber: '1',
          anchorString: '**signature**',
          anchorUnits: 'pixels',
          anchorXOffset: '10',
          anchorYOffset: '10',
        });

        const field1 = Text.constructFromObject({
          documentId: '1',
          pageNumber: '1',
          fontSize: 'size12',
          width: '118',
          height: '13',
          required: 'true',
          tabLabel: 'field1_text',
          tooltip: 'Required, text field',
          anchorString: '**text**',
        });

        const field2 = Text.constructFromObject({
          documentId: '1',
          pageNumber: '1',
          fontSize: 'size12',
          width: '118',
          height: '13',
          tabLabel: 'field2_text',
          tooltip: 'Prefilled',
          anchorString: '**prefill**',
        });

        const chkBox1 = Checkbox.constructFromObject({
            documentId: '1',
            pageNumber: '1',
            fontSize: 'size12',
            width: '12', // or 8
            height: '12',
            tabLabel: 'chkbox1',
            anchorString: 'chk1',
          }),
          chkBox2 = Checkbox.constructFromObject({
            documentId: '1',
            pageNumber: '1',
            fontSize: 'size12',
            width: '12', // or 8
            height: '12',
            tabLabel: 'chkbox2',
            anchorString: 'chk2',
          });

        const radioGroup = RadioGroup.constructFromObject({
          documentId: '1',
          groupName: 'radio1',
          radios: [
            Radio.constructFromObject({
              pageNumber: '1',
              value: 'red',
              fontSize: 'size12',
              width: '12', // or 8
              height: '12',
              anchorString: 'r1',
            }),
            Radio.constructFromObject({
              pageNumber: '1',
              value: 'blue',
              fontSize: 'size12',
              width: '12', // or 8
              height: '12',
              anchorString: 'r2',
            }),
          ],
        });

        // create tabs for signer1
        const signer1Tabs = Tabs.constructFromObject({
          signHereTabs: [signHere],
          textTabs: [field1, field2],
          checkboxTabs: [chkBox1, chkBox2],
          radioGroupTabs: [radioGroup],
        });
        signer1.tabs = signer1Tabs;
      }

      signers.push(signer1);
    }

    const parsedCcs = JSON.parse(options.ccs);
    const ccs = [];
    let lastRouteOrder = signers.length + 1,
      lastRecipientId = signers.length + 1;
    for (let index = 0; index < parsedCcs.length; index++) {
      const cc = parsedCcs[index];
      const cc1 = new CarbonCopy();
      cc1.roleName = 'cc' + index;
      cc1.name = cc.name;
      cc1.email = cc.email;
      cc1.recipientId = `${lastRouteOrder}`;
      cc1.routingOrder = `${lastRecipientId}`;
      lastRecipientId++, lastRouteOrder++;
      ccs.push(cc1);
    }

    // add recipients
    const recipients = Recipients.constructFromObject({
      signers: signers,
      carbonCopies: ccs,
    });

    // overall template definition
    const template = new EnvelopeTemplate.constructFromObject({
      // The order in the docs array determines the order in the env
      documents: [doc],
      emailSubject: options.subject,
      description: options.tempDescription,
      name: options.tempName,
      shared: 'false',
      recipients: recipients,
      status: 'created', // * sent - The envelope is sent to the recipients.  * created - The envelope is saved as a draft and can be modified and sent later.
    });

    return template;
  }

  updateTemplate(options) {
    // add or update ccs
    const template = new EnvelopeTemplate.constructFromObject({
      emailSubject: options.subject, // email subject when received
      description: options.tempDescription, // template description
      name: options.tempName, // template name
    });

    return template;
  }

  updateTemplateRecipients(newRecipients) {
    const recipients = new TemplateRecipients();

    const signers = [];
    for (let index = 0; index < newRecipients.signers.length; index++) {
      const signer = newRecipients.signers[index];
      const signer1 = new Signer();
      signer1.roleName = 'signer' + index;
      signer1.name = signer.name;
      signer1.email = signer.email;
      signer1.recipientId = `${index + 1}`;
      signer1.routingOrder = `${index + 1}`;
      signers.push(signer1);
    }

    const ccs = [];
    let lastRouteOrder = signers.length + 1,
      lastRecipientId = signers.length + 1;
    for (let index = 0; index < newRecipients.ccs.length; index++) {
      const cc = newRecipients.ccs[index];
      const cc1 = new CarbonCopy();
      cc1.roleName = 'cc' + index;
      cc1.name = cc.name;
      cc1.email = cc.email;
      cc1.recipientId = `${lastRouteOrder}`;
      cc1.routingOrder = `${lastRecipientId}`;
      lastRecipientId++, lastRouteOrder++;
      ccs.push(cc1);
    }

    recipients.signers = signers;
    recipients.carbonCopies = ccs;

    return recipients;
  }
}
