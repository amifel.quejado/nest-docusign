import { Type } from 'class-transformer';
import {
  IsOptional,
  IsNotEmpty,
  ValidateNested,
  IsAlphanumeric,
  IsEmail,
  IsString,
} from 'class-validator';

class Recipient {
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsOptional()
  @IsNotEmpty()
  @IsAlphanumeric()
  name: string;
}
export class SendEnvelopeDto {
  @IsNotEmpty()
  @IsString()
  templateId: string;

  @IsOptional()
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => Recipient)
  signer: Recipient;

  @IsOptional()
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => Recipient)
  cc: Recipient;
}
