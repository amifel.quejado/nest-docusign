import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'recipientsArray', async: false })
export class RecipientsArray implements ValidatorConstraintInterface {
  validate(recipients: string, args: ValidationArguments) {
    if (!JSON.parse(recipients)) return false;

    const recipientsArr = JSON.parse(recipients);
    recipientsArr.forEach((recipients) => {
      if (
        !recipients.hasOwnProperty('email') ||
        !recipients.hasOwnProperty('name')
      )
        return false;
      // TODO: validate name and email
    });
    return true;
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return `Invalid ${args.object} value`;
  }
}
