import {
  CACHE_MANAGER,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { Cache } from 'cache-manager';
import { DocusignService } from './docusign.service';
import { AuthRequest } from 'src/interfaces/request.interface';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
    private readonly docusignService: DocusignService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}
  async use(req: AuthRequest, res: Response, next: NextFunction) {
    const cachetoken = await this.cacheManager.get('token');

    if (!cachetoken) {
      await this.docusignService.login();
      // throw new HttpException('Invalid access token', HttpStatus.UNAUTHORIZED);
    }

    // TODO: Auto-generate new token on expire
    // Check request header if has Secret-Key equals to API_SECRET_KEY

    // const result = await this.docusignService.getUserLoggedIn(token);

    // if (result.status === 401) {
    //   const result = await this.docusignService.login();

    // }

    req.token = cachetoken;

    next();
  }
}
